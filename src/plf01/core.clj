(ns plf01.core)

;Bloque de Funciones para <
(defn función-<-1
  [a]
  (< a))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 2)
(función-<-2 2 4)
(función-<-3 2 4 5)

;Bloque de Funciones para <=

(defn función-<=-1
  [a]
  (<= a))

(defn función-<=-2
  [a b]
  (<= a b))

(defn función-<=-3
  [a b c]
  (<= a b c))

(función-<=-1 2)
(función-<=-2 2 4)
(función-<=-3 2 4 5)

;Bloque de funciones para ==

(defn función-==-1
  [a]
  (== a))

(defn función-==-2
  [a b]
  (== a b))

(defn función-==-3
  [a b c]
  (== a b c))

(función-==-1 2)
(función-==-2 2 4)
(función-==-3 2 4 5)

;Bloque de funciones para >

(defn función->-1
  [a]
  (> a))

(defn función->-2
  [a b]
  (> a b))

(defn función->-3
  [a b c]
  (> a b c))

(función->-1 2)
(función->-2 2 4)
(función->-3 2 4 5)

;Bloque de funciones para >=

(defn función->=-1
  [a]
  (>= a))

(defn función->=-2
  [a b]
  (>= a b))

(defn función->=-3
  [a b c]
  (>= a b c))

(función->=-1 2)
(función->=-2 2 4)
(función->=-3 2 4 5)

;Bloque de funciones para ASSOC

(defn función-assoc-1
   [a c d]
  (assoc a c d))

(defn función-assoc-2
  [a b c d e]
  (assoc a b c d e))

(defn función-assoc-3
  [a c d e f g h]
  (assoc a c d e f g h))


(función-assoc-1  {:key1 "old value1" :key2 "value2"} :key1  :key3)
(función-assoc-2 {:key1 "valor0" :k1y2 "ejem" :key3 "h" :key4 '(2 3) :key5 '(2 3)} :key1 "valorn 1" :key3 "Natali")
(función-assoc-3   {:key1 "old value1" :key2 "value2" :key3 "valor3" :key4 "Castillo"} :key1  "Mareli" :key3 "Natali" :key2 "Rojas")


;Bloque de funciones para ASSOC-IN

(defn función-assoc-in-1
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-2
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-3
  [a b c]
  (assoc-in a b c))

(función-assoc-in-1 {:id 23 :id2 24} [] {:P :L :F :1})
(función-assoc-in-2 {{:key0 "Mareli"} {:key "Natali"}} [0] :nombre)
(función-assoc-in-3 {:alumno {:numControl "17"}} [1] "31")


;Bloque de funciones para CONCAT

(defn función-concat-1
  [a]
  (concat a))

(defn función-concat-2
  [a b]
  (concat a b))

(defn función-concat-3
  [a b c]
  (concat a b c))

(función-concat-1 [1 "Mareli" true])
(función-concat-2 [1 2] '(3 4))
(función-concat-3 "abc" [5 6 7] #{9 10 8})


;Bloque de funciones para CONJ

(defn función-conj-1
  [a]
  (conj a))

(defn función-conj-2
  [a  b]
  (conj a b))

(defn función-conj-3
  [a  b c]
  (conj a b c))

(función-conj-1 '(a b c))
(función-conj-2 #{23 24 25} '(9 8 7))
(función-conj-3 [[9 8 7] [6 5 4]] [3 2 1] [11 12 12])

;Bloque de funciones para CONS

(defn función-cons-1
  [a b]
  (cons a b))

(defn función-cons-2
  [a b]
  (cons a b))

(defn función-cons-3
  [a b]
  (cons a b))

(función-cons-1 "M" "areli")
(función-cons-2 [3 4] [{5 6} {7 8}])
(función-cons-3 '(1 2 3) [["a" "b" "c"] {:key1 '(1 2 3)} '(1 2 3)])

;Bloque de funciones para CONTAINS?

(defn función-contains?-1
  [a b]
  (contains? a b))

(defn función-contains?-2
  [a b]
  (contains? a b))

(defn función-contains?-3
  [a b]
  (contains? a b))

(función-contains?-1 [\a \b] \c)
(función-contains?-2 {:key1 :key2} :key1)
(función-contains?-3 [\a \b \c \d] 5)

;Bloque de funciones para COUNT

(defn función-count-1
  [a]
  (count a))

(defn función-count-2
  [a]
  (count a))

(defn función-count-3
  [a]
  (count a))

(función-count-1 nil)
(función-count-2 '(45 "ma" \a [9 8]))
(función-count-3 ["PLF" '(\m \n \b) #{true false} [1 2 3] {:key1 :key2}])

;Bloque de funciones para DISJ

(defn función-disj-1
  [a b]
  (disj a b))

(defn función-disj-2
  [a b]
  (disj a b))

(defn función-disj-3
  [a b]
  (disj a b))

(función-disj-1 #{\a \b \c} \c)
(función-disj-2 #{90 "Natali" true [1 3]} true)
(función-disj-3 #{1 2 3 4 5} 3)

;Bloque de funciones para DISSOC

(defn función-dissoc-1
  [a]
  (dissoc a))

(defn función-dissoc-2
  [a b]
  (dissoc a b))

(defn función-dissoc-3
  [a h f]
  (dissoc a h f))

(función-dissoc-1 {:clave0 "pk" :clave1 "hg" :clave2 "pj"})
(función-dissoc-2 {:key1 \a :key2 [1 2 3] :key3 3} :key2)
(función-dissoc-3 {:v1 '("a" "z" "b") :v2 [9 8 7] :v3 true :v4 \n} :v3 :v1)

;Bloque de funciones para DISTINCT

(defn función-distinct-1
  [a]
  (distinct a))

(defn función-distinct-2
  [a]
  (distinct a))

(defn función-distinct-3
  [a]
  (distinct a))

(función-distinct-1 [true false \a \b \c true \a 34])
(función-distinct-2 [["a" "f" "g" "a" "b" "g"] ["a" "f" "g" "a" "b" "g"]])
(función-distinct-3 [[3 4] '(true false) [3 4] [2 1] [2 1] [33 44 5] [\a]])

;Bloque de funciones para DISTINCT?
(defn función-distinct?-1
  [a]
  (distinct? a))

(defn función-distinct?-2
  [a b]
  (distinct? a b))

(defn función-distinct?-3
  [a]
  (distinct? a))

(función-distinct?-1 77)
(función-distinct?-2 12 12)
(función-distinct?-3 [[9 8 7] [22 33 44] [1 2 3] [8 3 9]])

;Bloque de funciones para DROP-LAST?
(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a]
  (drop-last a))

(función-drop-last-1 [1 4 7 8])
(función-drop-last-2 2 [9 8 7 6])
(función-drop-last-3 [[9 8 7 6] [83 59 5] [6 7 8] [23 45]])

;Bloque de funciones para EMPTY
(defn función-empty-1
  [a]
  (empty a))

(defn función-empty-2
  [a]
  (empty a))

(defn función-empty-3
  [a]
  (empty a))

(función-empty-1 #{45 true \a})
(función-empty-2 '(34 [345]))
(función-empty-3 ['(5 7 8) [3 6 7] \m])


;Bloque de funciones para EMPTY?

(defn función-empty?-1
  [a]
  (empty? a))

(defn función-empty?-2
  [a]
  (empty? a))

(defn función-empty?-3
  [a]
  (empty? a))

(función-empty?-1 ())
(función-empty?-2 '(34 [345]))
(función-empty?-3 ['(5 7 8) [3 6 7] \m])


;Bloque de funciones para EVEN?

(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [b]
  (even? b))

(defn función-even?-3
  [b]
  (even? b))

(función-even?-1 8)
(función-even?-2 25)
(función-even?-3 102)

;Bloque de funciones para FALSE?

(defn función-false?-1
  [a]
  (false? a))

(defn función-false?-2
  [a]
  (false? a))

(defn función-false?-3
  [a]
  (false? a))

(función-false?-1 false)
(función-false?-2 [true])
(función-false?-3 '([45 45 3]))

;Bloque de funciones para FIND

(defn función-find-1
  [a b]
  (find a b))

(defn función-find-2
  [a b]
  (find a b))

(defn función-find-3
  [a b]
  (find a b))

(función-find-1 {:p1 234} :m3)
(función-find-2 {{:p1 234 :p2 856} {:m1 \a :m2 \m}} :p1)
(función-find-3  {:p1 234 :p2 \a } :p1)


;Bloque de funciones para FIRST

(defn función-first-1
  [a]
  (first a))

(defn función-first-2
  [a]
  (first a))

(defn función-first-3
  [a]
  (first a))

(función-first-1 ())
(función-first-2 ['(6 9) #{true false}])
(función-first-3 [[\n \a \t] '(7 3) {:v "go" :v1 "te"} #{4 5 99}])

;Bloque de funciones para flatten

(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a]
  (flatten a))

(función-flatten-1 '(\a))
(función-flatten-2 [\a '(true false)])
(función-flatten-3 [\a true 99 "h"])

;Bloque de funciones para frequencies

(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [a]
  (frequencies a))

(defn función-frequencies-3
  [a]
  (frequencies a))

(función-frequencies-1 ["p3" \h 4 9 4 'r])
(función-frequencies-2 [\a true])
(función-frequencies-3 [6 6 9])

;Bloque de funciones para get

(defn función-get-1
  [a b]
  (get a b))

(defn función-get-2
  [a b]
  (get a b))

(defn función-get-3
  [a b]
  (get a b))

(función-get-1 ['a 'e 'i 'o 'u] 1)
(función-get-2 [false 78]4)
(función-get-3 {:value 9 :v 78} :v)

;Bloque de funciones para get-in

(defn función-get-in-1
  [a b]
  (get-in a b))

(defn función-get-in-2
  [a b]
  (get-in a b))

(defn función-get-in-3
  [a b]
  (get-in a b))

(función-get-in-1 [[1 2 3]
                   [4 5 6]
                   [7 8 9]] [0 1])
(función-get-in-2 [[\k \f] [9 4]] [1 0])
(función-get-in-3 [[\k \f] [9 4] [1 0]] [2 3])

;Bloque de funciones para INTO

(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

(función-into-1 ['o 'u 'i] ['e 'a])
(función-into-2 ['(4 9 2) [2 9 8]] [3])
(función-into-3 [#{true false} [23 27 28] '('a 'e)][7 2])

;Bloque de funciones para KEY

(defn función-key-1
  [a]
  (map key a))

(defn función-key-2
  [a]
  (map key a))

(defn función-key-3
  [a]
  (map key a))

(función-key-1 {:d 9 :ip \f :v 't :r 9})
(función-key-2 {:x 89})
(función-key-3 {:val1 78 :ok 45 :id '23})

;Bloque de funciones para KEYS

(defn función-keys-1
  [a]
  (keys a))

(defn función-keys-2
  [a]
  (keys a))

(defn función-keys-3
  [a]
  (keys a))

(función-keys-1 ())
(función-keys-2 {:d 3 :b 3})
(función-keys-3 {'r 4 'k 2 'o 6})

;Bloque de funciones para MAX

(defn función-max-1
  [a]
  (max a))

(defn función-max-2
  [a b c]
  (max a b c))

(defn función-max-3
  [a b c]
  (max a b c))


(función-max-1 10)
(función-max-2 24 90 333)
(función-max-3 6 9 12)

;Bloque de funciones para MERGE
(defn función-merge-1
  [a]
  (merge a))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b c]
  (merge a b c))

(función-merge-1 {:clave "ok"})
(función-merge-2 {'p 6 'k 2 'f 8} {:d 23 :kl 29})
(función-merge-3 {:a 1 :d 6} {:b 1 :v 9} {:c 1 :k 9})

;Bloque de funciones para MIN

(defn función-min-1
  [a]
  (min a))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a b c]
  (min a b c))

(función-min-1 10)
(función-min-2 24 90 333)
(función-min-3 6 9 12)

;Bloque de funciones para NEG? 

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a]
  (neg? a))

(defn función-neg?-3
  [a]
  (neg? a))

(función-neg?-1 100)
(función-neg?-2 -100)
(función-neg?-3 -3844)

;Bloque de funciones para NIL? 

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [a]
  (nil? a))

(función-nil?-1 100)
(función-nil?-2 nil)
(función-nil?-3 -23)

;Bloque de funciones para not-empty

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 nil)
(función-not-empty-2 {:id 4 :v 0})
(función-not-empty-3 [9 8 5])


;Bloque de funciones para nth

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b c]
  (nth a b c))

(defn función-nth-3
  [a b c]
  (nth a b c))

(función-nth-1 [7 3 4 5] 3)
(función-nth-2 [8 6 7 0] 187 56)
(función-nth-3 ["Mareli"] -1 "Natali")


;Bloque de funciones para odd?

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 2)
(función-odd?-2 9)
(función-odd?-3 1997)

;Bloque de funciones para partition

(defn función-partition-1
  [a b]
  (partition a b))

(defn función-partition-2
  [a b ]
  (partition a b ))

(defn función-partition-3
  [a b c d]
  (partition a b c d))

(función-partition-1 3 (range 30))
(función-partition-2 4 (range 6 20))
(función-partition-3 5 7 [\a \b \c] (range 22))

;Bloque de funciones para partition-all

(defn función-partition-all-1
  [a b]
  (partition a b))

(defn función-partition-all-2
  [a b]
  (partition a b))

(defn función-partition-all-3
  [a b]
  (partition a b))

(función-partition-all-1 4 [9 8 7 6 5 4 12 34 78 9])
(función-partition-all-2 2 [3 4 6 7 8 9 2 3 4 0 9 1 28 374 84 9])
(función-partition-all-3 5 [1 2 3 4 5 6 7 8 9 10])

;Bloque de funciones para peek

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [b]
  (peek b))

(defn función-peek-3
  [b]
  (peek b))

(función-peek-1 [5 's 4 'f 6 'j 7])
(función-peek-2 '(5 's 4 'f 6 'j 7))
(función-peek-3 nil)

;Bloque de funciones para pop
(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [b]
  (pop b))

(defn función-pop-3
  [c]
  (pop c))

(función-pop-1 [5 's 4 'f 6 'j 7])

(función-pop-2 '(5 's 4 'f 6 'j 7))

(función-pop-3 nil)

;Bloque de funciones para pos?

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a]
  (pos? a))

(defn función-pos?-3
  [a]
  (pos? a))

(función-pos?-1 2989)
(función-pos?-2 -234)
(función-pos?-3 0)

;Bloque de funciones para quot

(defn función-quote-1
  [a]
  (quote a))

(defn función-quote-2
  [b]
  (quote b))

(defn función-quote-3
  [c]
  (quote c))

(función-quote-1 nil)
(función-quote-2 8)
(función-quote-3 1)

;Bloque de funciones para range

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))

(función-range-1 35)
(función-range-2 -10 10)
(función-range-3 -50 50 5)

;Bloque de funciones para rem

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [a b]
  (rem a b))


(función-rem-1 50 4)
(función-rem-2 -9 3)
(función-rem-3 -15 7)

;Bloque de funciones para repeat

(defn función-repeat-1
  [a b]
  (repeat a b))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b]
  (repeat a b))

(función-repeat-1 3 "ya merito")
(función-repeat-2 5 [3 9 4 \a true])
(función-repeat-3 10 '('a 'e 'i o u))


;Bloque de funciones para replace

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))


(función-replace-1 [7 8 5 6 \b \j] [0 3 2])
(función-replace-2 {3 \b, 5 true, 6 'j} [5 3 4 6 7 10])
(función-replace-3 {:v1 88 :dw \a} [:v1 :dw])


;Bloque de funciones para rest

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [b]
  (rest b))

(defn función-rest-3
  [c]
  (rest c))

(función-rest-1 '(\r \t \v \w \x))
(función-rest-2 [89347 85960 45294 8560])
(función-rest-3 nil)

;Bloque de funciones para select-keys

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys a b))

(función-select-keys-1 {:key1 "hols" :key2 "palabra" :key3 "clave"} [:key2 :key3])
(función-select-keys-2 {:key1 \d :key2 '() :key3 [2 4 5]} [:key2 :key3])
(función-select-keys-3{ :identificador "Hombre" :sexo \M} [:identificador])

;Bloque de funciones para shuffle

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle a))


(función-shuffle-1 [8 7 6 5 9 12 45 78])
(función-shuffle-2 (list 99 89 97 95 94))
(función-shuffle-3 [[7 8 9][23 45][12 13 14 14][9 2]])

;Bloque de funciones para sort

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort a))

(defn función-sort-3
  [b]
  (sort > b))

(función-sort-1 [9 6 5 234 9898 0])
(función-sort-2 [468 384 62 48 47])
(función-sort-3 [9 6 5 234 9898 0])


;Bloque de funciones para split-at

(defn función-split-at-1
  [a b ]
  (split-at a b))

(defn función-split-at-2
  [a b]
  (split-at a b))

(defn función-split-at-3
  [a b]
  (split-at a b))
(función-split-at-1 3 [78 9 5 7 8 4 8 4])
(función-split-at-2 6 [9 8 7 88 2 45 6 7 8 1 23 5 6 8])
(función-split-at-3 2 [84 3 22 5 9 10 0])

;Bloque de funciones para str

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a b ]
  (str a b))

(defn función-str-3
  [a b c]
  (str a b c))

(función-str-1 202)
(función-str-2 true [23 89])
(función-str-3 "leyendas de funciones" '() [])

;Bloque de funciones para subs

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b c]
  (subs a b c))

(función-subs-1 "Materia" 4)
(función-subs-1 "Materia" 1)
(función-subs-2 "Programación lógica y funcional" 13 19)

;Bloque de funciones para subvec

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b c]
  (subvec a b c))

(función-subvec-1 [\a 3 5 9 '(2 6 7) ["a" "v"] ] 3)
(función-subvec-1 [8 6 4 9 23 4 67 6 23 4 89 2 4 5 6] 5)
(función-subvec-2 ["a" true 897 #{23 9 87} 45 6] 3 4)

;Bloque de funciones para take

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 4 #{1 2 3 4 5 6})
(función-take-2 1 ['("n" "m") [45 7 9 34]])
(función-take-3 2 {{3 6 7 8} {23 4 5 9}})


;Bloque de funciones para true?

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 true)
(función-true?-2 45)
(función-true?-3 (< 2 9))

;Bloque de funciones para val

(defn función-val-1
  [a]
  (map val a))

(defn función-val-2
  [a ]
  (map val a))

(defn función-val-3
  [e]
  (val (first e)))

(función-val-1 {:k 89 :p "MN" :w '(2 3 5)})
(función-val-2 {:4j 45 :s 456})
(función-val-3  {:k 89 :p "MN" :w '(2 3 5)})

;Bloque de funciones para vals

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))


(función-vals-1 [])
(función-vals-2 {:z "mare", :w "natali", :y "rojas", :v "castillo"})
(función-vals-3 {:k #{8 9 3} :p \a :w '(24 35 56)})

;Bloque de funciones para zero?

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

(función-zero?-1 0)
(función-zero?-2 (+ 0 0))
(función-zero?-3 234)

;Bloque de funciones para zipmap

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1 [:key1 :key2 :key3 :key4] [[3 5 6] \f '(3 4 8) "m"])
(función-zipmap-2 [:m :4] [345 \a])
(función-zipmap-3 [:val1 :val2 :val3 :val4] [#{4 5 7} '("a" "b") \f 3456])

















































































































